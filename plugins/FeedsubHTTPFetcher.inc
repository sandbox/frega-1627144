<?php

/**
 * @file
 * Fetches data from an SQL database.
 */

/**
 * Fetches data via pdo connection.
 */
class FeedsubHTTPFetcher extends FeedsHTTPFetcher {
  /**
     * Implements FeedsFetcher::fetch().
     */
    public function fetch(FeedsSource $source) {
      $source_config = $source->getConfigFor($this);
      return new FeedsHTTPFetcherResult($this->rewriteUrl($source_config['source']));
    }

    /**
     * Clear caches.
     */
    public function clear(FeedsSource $source) {
      $source_config = $source->getConfigFor($this);
      $url = $source_config['source'];
      $url = $this->rewriteUrl($url);
      feeds_include_library('http_request.inc', 'http_request');
      http_request_clear_cache($url);
    }

    /**
     * Implements FeedsFetcher::request().
     */
    public function request($feed_nid = 0) {
      drupal_access_denied();
    }

    /**
     * Override parent::configDefaults().
     */
    public function configDefaults() {
      return array();
    }

    /**
     * Override parent::configForm().
     */
    public function configForm(&$form_state) {
      $form = array();
      return $form;
    }

    /**
     * Override sourceSave() - subscribe to hub.
     */
    public function sourceSave(FeedsSource $source) {
      return parent::sourceSave($source);
    }

    /**
     * Override sourceDelete() - unsubscribe from hub.
     */
    public function sourceDelete(FeedsSource $source) {
      return parent::sourceDelete($source);
    }


    function rewriteUrl($url) {
      $info = dnode_get_dnode_info('feedsub');
      return 'http://flinker:5111/' . $url;
    }

}
