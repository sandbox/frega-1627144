var FeedsubscriptionManager = require( __dirname + '/lib/FeedsubscriptionManager.js').FeedsubscriptionManager;

// load config - @todo: proper configuration injection
if (typeof DrupalDnode !== 'undefined') {
  var config = DrupalDnode.getModuleConfig('feedsub');
} else {
  var config = require('./config.json');
}

var debug = config.debug || true;

var fsm = new FeedsubscriptionManager(config);

exports.addFeed = function(name, url, options, cb) {
  fsm.addFeed(name, url, options || {}, cb);
};

exports.hasFeed = function(name, cb) {
  fsm.hasFeed(name, cb);
};

exports.removeFeed = function(name, cb) {
  fsm.removeFeed(name, cb);
};

exports.getFeeds = function(cb) {
  fsm.getFeeds(cb);
};

exports.getArticles = function(sinceTs, cb) {
  var articles = [];
  var queuedItem;
  for (var i= 0, len=fsm.articleQueue.length; i<len; i++) {
    queuedItem = fsm.articleQueue[i];
    if (sinceTs && sinceTs < queuedItem.timestamp)  {
      break;
    } else {
      articles.push(queuedItem);
    }
  }
  cb(null, articles);
};

// add "http" proxy server
if (config.httpProxyPort) {
  var FeedsubscriptionHttpProxy = require('./lib/FeedsubscriptionHttpProxy');
  FeedsubscriptionHttpProxy.createFeedsubscriptionHttpProxyServer(fsm, config);
}

// just add some example
fsm.on('article', function(article, feed) {
  debug && console.log('FEED %s - new article: %s (%s)', feed.name, article.title, article.guid);
  DrupalDnode.connectByServerId('dnodeq', function(remote, conn) {
    // sendMessageToDrupal
    remote.sendMessageToDrupal('feedsub', {
      article: article,
      feedname: feed.name
    },
    // no particular options
    {},
    // callback, terminating the dnode-connection
    function(err, data) {
      conn.end();
    });
  });
});
// start ...
fsm.start();
fsm.poll();