var FeedParser = require('feedparser'),
  request = require('request'),
  _ = require('underscore'),
  events = require('events'),
  util = require('util');

function FeedsubscriptionManager(options) {
  this.options = options || {};
  this.debug = options.debug || false;
  // @todo persist and load
  this.feeds = {};
  this.articleQueue = [];
  events.EventEmitter.call(this);
}
util.inherits(FeedsubscriptionManager, events.EventEmitter);

FeedsubscriptionManager.prototype.addFeed = function(name, url, options, cb) {
  options = _.defaults(options || {}, {
    'historyLength': 50,
    'pollInterval': this.options.pollInterval
  });
  this.feeds[name] = {
    name: name,
    url: url,
    options: options,
    //
    requestInfo: {},
    history: []
  };
  cb && cb(null);
};

FeedsubscriptionManager.prototype.hasFeed = function(name, cb) {
  cb(null, typeof this.feeds[name] !== 'undefined' ? this.feeds[name] : false);
};

FeedsubscriptionManager.prototype.removeFeed = function(name, cb) {
  delete this.feeds[name];
  cb && cb(null);
};

FeedsubscriptionManager.prototype.getFeeds = function(cb) {
  cb(null, this.feeds);
};

FeedsubscriptionManager.prototype.poll = function(cb) {
  var feed, reqObj;
  var r = {};
  var self = this;
  _.each(this.feeds, function(feed, name) {
    this.debug && console.log('Polling feed', feed, name);
    if (!feed.requestInfo.lock ) {
      var needsPoll = (
        !feed.requestInfo.lastPoll
          ||
        !feed.options.pollInterval
          ||
        (feed.options.pollInterval && ( (+ new Date()) - feed.requestInfo.lastPoll > feed.options.pollInterval))
      );
      if (needsPoll) {
        feed.requestInfo.lastPoll = feed.requestInfo.lock = +(new Date());
        reqObj = {
          'uri': feed.url
        };
        // let's be not too stoopid ...
        if (feed.requestInfo.lastModified) {
          reqObj['If-Modified-Since'] = feed.requestInfo.lastModified;
        }
        if (feed.requestInfo.etag) {
          reqObj['If-None-Match'] = feed.requestInfo.etag;
        }
        var parser = new FeedParser();
        // store a reference to the active feed in the parser.
        parser.feed = feed;
        parser.on('article', function (article) {
          var feed = this.feed;
          var feedname = feed.name;
          var type;
          if (article.guid && feed.history.indexOf(article.guid) === -1) {
            type = 'new-article';
            feed.history.push(article.guid);
            if (feed.history.length > feed.options.historyLength) {
              feed.history.shift();
            }
            self.emit('new-article', article, feed);
          } else {
            type = 'old-article';
            self.emit('old-article', article, feed);
          }
          self.debug && console.log('feed %s - %s article: %s (%s)', feed.name, type, article.title, article.guid);
          self.emit('article', article, feed);
        });
        this.debug && console.log('Preparing poll %s (%j)', feed.name, reqObj);
        // @todo: piping streams instead (sexier and more efficient).
        request(reqObj, function (err, response, body) {
          if (!err) {
            var feed = parser.feed;
            self.debug && console.log('Completed request %s', feed.name);
            // @todo: handle errors, i.e. back off or something akin.
            feed.requestInfo.counter = feed.requestInfo.counter ? feed.requestInfo.counter++ : 1;
            feed.requestInfo.lock = false;
            feed.requestInfo.lastModified = response.headers['last-modified'];
            feed.requestInfo.etag = response.headers['etag'] ? response.headers['etag'] : null;
            // store the response to be able to 'proxy' it.
            feed.requestInfo.response = {
              'body': body,
              'headers': response.headers
            };
            parser.parseString(body);
          } else {
            self.debug && console.log('Err', err);
          }
        });
        this.debug && console.log('Sent request %s', feed.name);
      } else {
        this.debug && console.log('No polling required currently %s', feed.name);
      }
    } else {
      // @todo - we might want to handle backing off etc.
      this.debug && console.log('Polling lock on feed %s (%j)', name, feed);
    }
  });
  cb && cb(null);
}

FeedsubscriptionManager.prototype.start = function() {
  var self = this;
  this._interval = setInterval( function(self) {
    self.poll(function() {
      self.debug && console.log('Completed loop', util.inspect(process.memoryUsage()));
    });
  }, this.options.pollInterval, this);
}

FeedsubscriptionManager.prototype.stop = function() {
  this._interval && clearInterval(this._interval);
}


exports.FeedsubscriptionManager = FeedsubscriptionManager;