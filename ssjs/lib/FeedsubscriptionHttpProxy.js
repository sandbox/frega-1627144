var http = require('http');
var url = require('url');

exports.createFeedsubscriptionHttpProxyServer = function(fsm, config) {
  var debug = config.debug || false;
  this.httpServer = http.createServer(function (req, res) {
    // strip the
    var ui = url.parse(req.url);
    // strip /
    var feedUrl = ui.href.substr(1);
    // make sure we have rem
    var feedUrlInfo = url.parse(feedUrl);
    // let's check only for proper URI
    if (!feedUrlInfo.host || !feedUrlInfo.protocol || feedUrlInfo.protocol.indexOf('http') == -1) {
      debug && console.log('Ignoring request', feedUrl);
      res.writeHead(400, {'Content-Type': 'text/plain'});
      res.write('Bad request');
      res.end();
    } else {
      // @todo: delete a feed?
      fsm.hasFeed(feedUrl, function(err, feed) {
        if (!feed) {
          fsm.addFeed(feedUrl, feedUrl, {}, function(err, info) {
            debug && console.log('added new feed '  + feedUrl);
            // @todo: maybe redirect to the remoteUrl?
            // 307 temporary redirect
            res.writeHead(307, {'Location': feedUrl});
            res.end();
            /* res.writeHead(200, {'Content-Type': 'text/plain'}); */
          });
        } else if (!feed.requestInfo || !feed.requestInfo.response) {
          debug && console.log('feed has not been added but not fetched '  + feedUrl)
          // @todo: maybe redirect to the remoteUrl?
          res.statusCode = 204;
          res.end();
        } else {
          debug &&  console.log('Returning cached feed data for '  + feedUrl);
          for (var header in feed.requestInfo.response.headers) {
            res.setHeader(header, feed.requestInfo.response.headers[header]);
          }
          res.write(feed.requestInfo.response.body);
          res.end();
          /* debug && console.log('headers', feed.requestInfo.response.headers, 'body', feed.requestInfo.response.body);*/
        }
      });
    }
  }).listen(config.httpProxyPort);
  debug && console.log('feedsub - started http-proxy', config.httpProxyPort);
};

